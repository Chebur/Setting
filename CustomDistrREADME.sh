#https://gitlab.com/Chebur70
#make on the platform Bullseye
#https://github.com/kachnu
#https://github.com/kachnu/my_script
"CustomDistr-создание своего iso"

#Install CustomDistrAppimage
			        echo "================ Установка CustomDistrAppimage ================" 
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/CustomDistrAplPng.tar.gz                             
                                tar xvf CustomDistrAplPng.tar.gz && rm -rf CustomDistrAplPng.tar.gz
                                chmod +x CustomDistrAplPng/CustomDistrAppimage.desktop                                                         
                                cp CustomDistrAplPng/CustomDistrAppimage.desktop ~/.local/share/applications/
                                sudo cp CustomDistrAplPng/CustomDistr.png /usr/share/icons/   
				chmod +x CustomDistrAplPng/CustomDistr.AppImage                        
                                sudo cp CustomDistrAplPng/CustomDistr.AppImage /usr/local/bin/
				cp CustomDistrAplPng/mydistrREADME.sh ~/'Рабочий стол'/   или   ~/'Desktop'/
                                rm -rf CustomDistrAplPng
				
#Install CustomDistrSh
			        echo "================ Установка CustomDistrSh ================" 
                                sudo apt install -y squashfs-tools rsync beep
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/CustomDistrAplPng.tar.gz                               
                                tar xvf CustomDistrAplPng.tar.gz && rm -rf CustomDistrAplPng.tar.gz
                                chmod +x CustomDistrAplPng/CustomDistr.desktop                                                            
                                cp CustomDistrAplPng/CustomDistr.desktop ~/.local/share/applications/
                                sudo cp CustomDistrAplPng/CustomDistr.png /usr/share/icons/ 
				chmod +x CustomDistrAplPng/CustomDistr.sh                            
                                sudo cp CustomDistrAplPng/CustomDistr.sh /usr/local/bin/
				cp CustomDistrAplPng/mydistrREADME.sh ~/'Рабочий стол'/   или   ~/'Desktop'/
                                rm -rf CustomDistrAplPng
				
###############################################################################
Следующая последовательность команд написана в качестве мануала и нужна для установки ПО в новой сборке.
 
cd mydistr
sudo -s

cp /etc/apt/apt.conf.d/proxy mydistr_root/etc/apt/apt.conf.d/
cp /etc/hosts mydistr_root/etc/
cp /etc/resolv.conf mydistr_root/etc/
mount --bind /dev/ mydistr_root/dev
chroot mydistr_root

mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C
export LANG=
dbus-uuidgen > /var/lib/dbus/machine-id
dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl


Издеваемся над дистром, ставим программы apt-get update upgrade dist-upgrade purge install -f или aptitude

apt-get clean
rm -rf /tmp/* ~/.bash_history /home/*
rm /var/lib/dbus/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl
umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
exit

umount mydistr_root/dev
rm -rf mydistr_root/run/synaptic.socket
rm mydistr_root/etc/hosts
rm mydistr_root/etc/resolv.conf
rm mydistr_root/etc/apt/apt.conf.d/proxy

