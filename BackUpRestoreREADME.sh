#https://github.com/tritonas00/system-tar-and-restore
#https://mxrepo.com/mx/repo/pool/main/g/gtkdialog/
Created with gtkdialog_0.8.3-2mx23+1_amd64.deb 
"BackUpRestore-7.0-системы Linux"
#Install BackUpRestore.AppImage-7.0
			        echo "================ Установка BackUpRestore.AppImage-7.0 ================"
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/BackUpRestoreAppImage.tar.gz	
			        tar xvf BackUpRestoreAppImage.tar.gz && rm -rf BackUpRestoreAppImage.tar.gz
				chmod +x BackUpRestore/*.AppImage
                                sudo cp BackUpRestore/BackUpRestore.AppImage /usr/local/bin/       
			        chmod +x BackUpRestore/*.desktop
			        cp BackUpRestore/BackUpRestoreAppImage.desktop ~/.local/share/applications/
			        sudo cp BackUpRestore/BackUpRestore.png /usr/share/icons/	
				cp BackUpRestore/BackUp.png ~/'Рабочий стол'/	 или     ~/'Desktop'/
				cp BackUpRestore/Restore.png ~/'Рабочий стол'/	 или     ~/'Desktop'/
				cp BackUpRestore/BackUpRestoreUninstall.sh ~/'Рабочий стол'/	 или    ~/'Desktop'/ 	 					       
			        rm -Rf BackUpRestore

#Install script amd64 DebianTrixie
#Install BackUpRestoreSh-7.0
			        echo "================ Установка BackUpRestoreSh-7.0 ================"
				wget https://codeberg.org/Chebur/Setting/raw/branch/main/libglade2-0_2.6.4-2.4_amd64.deb
				wget https://codeberg.org/Chebur/Setting/raw/branch/main/gtkdialog_0.8.3-2mx21+2_amd64.deb
                                sudo dpkg -i *.deb
                                sudo apt install -fy
				sudo apt autoremove -y
                                rm -rf gtkdialog* libglade2*
                                sudo apt install -y git tar rsync wget gdisk openssl gzip bzip2 xz-utils pigz pbzip2 pixz
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/BackUpRestoreSh.tar.gz
                                tar xvf BackUpRestoreSh.tar.gz && rm -rf BackUpRestoreSh.tar.gz
				chmod +x BackUpRestore/*.desktop				
                                cp BackUpRestore/BackUpRestore.desktop ~/.local/share/applications/
				cp BackUpRestore/BackUpRestoreTerminal.desktop ~/.local/share/applications/
                                sudo cp -R BackUpRestore/BackUp /usr/share/icons/
				chmod +x BackUpRestore/BackUpRestore/*.sh
				sudo cp -R BackUpRestore/BackUpRestore /usr/local/bin/
				cp BackUpRestore/BackUp.png ~/'Рабочий стол'/	 или    ~/'Desktop'/
				cp BackUpRestore/Restore.png ~/'Рабочий стол'/	 или    ~/'Desktop'/
				cp BackUpRestore/BackUpRestoreUninstall.sh ~/'Рабочий стол'/	 или    ~/'Desktop'/ 
			        rm -Rf BackUpRestore
                                 			
#Install deb amd64
#Install BackUpRestoreDeb-7.0
			        echo "================ Установка BackUpRestoreDeb-7.0 ================"
                                sudo apt install -y git tar rsync wget gdisk openssl gzip bzip2 xz-utils pigz pbzip2 pixz 
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/gtkdialog_0.8.3-2mx19_amd64.deb
                                sudo dpkg -i gtkdialog*
                                sudo apt install -fy
                                rm -rf gtkdialog*
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/backuprestore_7.0_all.deb
                                sudo dpkg -i backuprestore*
                                sudo apt install -fy
                                rm -rf backuprestore*				

#Install script i386
#Install BackUpRestoreSh-7.0
			        echo "================ Установка BackUpRestoreSh-7.0 ================"
				wget https://codeberg.org/Chebur/Setting/raw/branch/main/gtkdialog_0.8.3-2mx19_i386.deb
                                sudo dpkg -i gtkdialog*
                                sudo apt install -fy
                                rm -rf gtkdialog*
                                sudo apt install -y git tar rsync wget gdisk openssl gzip bzip2 xz-utils pigz pbzip2 pixz
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/BackUpRestoreSh.tar.gz 
                                tar xvf BackUpRestoreSh.tar.gz && rm -rf BackUpRestoreSh.tar.gz
				chmod +x BackUpRestore/*.desktop				
                                cp BackUpRestore/BackUpRestore.desktop ~/.local/share/applications/
				cp BackUpRestore/BackUpRestoreTerminal.desktop ~/.local/share/applications/
                                sudo cp -R BackUpRestore/BackUp /usr/share/icons/
				chmod +x BackUpRestore/BackUpRestore/*.sh
				sudo cp -R BackUpRestore/BackUpRestore /usr/local/bin/
				cp BackUpRestore/BackUp.png ~/'Рабочий стол'/	 или    ~/'Desktop'/
				cp BackUpRestore/Restore.png ~/'Рабочий стол'/	 или    ~/'Desktop'/
				cp BackUpRestore/BackUpRestoreUninstall.sh ~/'Рабочий стол'/	 или    ~/'Desktop'/   
			        rm -Rf BackUpRestore
                                 				
#Install deb i386
#Install BackUpRestoreDeb-7.0
			        echo "================ Установка BackUpRestoreDeb-7.0 ================"
                                sudo apt install -y git tar rsync wget gdisk openssl gzip bzip2 xz-utils pigz pbzip2 pixz 
                                wget https://codeberg.org/Chebur/Setting/raw/branch/main/gtkdialog_0.8.3-2mx19_i386.deb
                                sudo dpkg -i gtkdialog*
                                sudo apt install -fy
                                rm -rf gtkdialog*
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/backuprestore_7.0_all.deb
                                sudo dpkg -i backuprestore*
                                sudo apt install -fy
                                rm -rf backuprestore*				

#Uninstall script i386 amd64
#Uninstall BackUpRestoreSh-7.0
                                echo "================ Удаление ScriptBackUpRestore-7.0 ================"
                                rm -Rf ~/.local/share/applications/BackUp
				sudo rm -rf /usr/share/icons/BackUpRestore.png
				sudo rm -rf /usr/share/icons/BackUpRestoreTerminal.png
				sudo rm -Rf /usr/local/bin/BackUpRestore
				sudo apt purge -y gtkdialog*
                                sudo apt autoremove

#Uninstall BackUpRestore.AppImage-7.0
                                echo "================ Удаление BackUpRestore.AppImage-7.0 ================"
                                rm -Rf ~/.local/share/applications/BackUpRestoreAppImage.desktop
				sudo rm -rf /usr/share/icons/BackUpRestoreAppImage.desktop
				sudo rm -Rf /usr/local/bin/BackUpRestore.AppImage
                                sudo apt autoremove

################################################################
https://github.com/tritonas00/system-tar-and-restore
https://www.tecmint.com/system-tar-and-restore-a-linux-backup-script/
'klichalex. Linux - это просто.'
138-а Резервное копирование Linux, (на примере Debian). Часть 1. Резервирование.
https://www.youtube.com/watch?v=k9nuum1dycY&ab_channel=klichalex.Linux-%D1%8D%D1%82%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE.
138-б Резервное копирование Linux, (на примере Debian). Часть 2. Восстановление.
https://www.youtube.com/watch?v=xQc6dXl2pjI&ab_channel=klichalex.Linux-%D1%8D%D1%82%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE.
################################################################
'Github: https://github.com/tritonas00/system-tar-and-restore   ' 
System Tar and Restore - универсальный сценарий резервного копирования системы для Linux
Требования:
gtkdialog 0.8.3 или новее (для графического интерфейса).
tar 1.27 или новее (поддержка acls и xattrs).
rsync (для режима передачи).
wget (для скачивания резервных архивов).
gptfdisk / gdisk (для GPT и Syslinux).
openssl / gpg (для шифрования).
Как установить System Tar и Restore Tool в Linux
Чтобы установить программу System Tar and Restore , 
вам необходимо сначала установить все необходимые программные пакеты, перечисленные ниже.
'sudo apt install -y git tar rsync wget gptfdisk openssl'
sudo apt install -y git tar rsync wget openssl [В Debian / Ubuntu ]
# yum install git tar rsync wget gptfdisk openssl [в CentOS / RHEL ]
# dnf install git tar rsync wget gptfdisk openssl [в Fedora ]
После того, как все необходимые пакеты установлены, пришло время загрузить эти сценарии, клонировав системный tar, 
восстановить репозиторий в вашей системе и запустить эти сценарии с правами пользователя root, 
в противном случае используйте команду sudo .
cd Download
git clone https://github.com/tritonas00/system-tar-and-restore.git
cd system-tar-and-restore/
ls
Установите System Tar и восстановите
Установите System Tar и восстановите
Резервное копирование системы Linux
Сначала создайте каталог, в котором будут храниться файлы резервных копий вашей системы 
(вы можете использовать любой другой каталог по вашему выбору).
sudo mkdir /backups
Теперь выполните следующую команду, 
чтобы создать файл резервной копии системы в /backupsкаталоге, файл архива будет сжат с помощью утилиты xz , где находятся флаги.
-i- указывает режим работы ( 0 означает режим резервного копирования).
-d - указывает целевой каталог, в котором будет храниться файл резервной копии.
-c - определяет утилиту сжатия.
-u - позволяет читать дополнительные параметры tar / rsync.
sudo ./star.sh -i 0 -d / backups -c xz -u "--warning = none"
Выполните резервное копирование системы Linux
Выполните резервное копирование системы Linux
Чтобы исключить из /homeрезервной копии, добавьте -Hфлаг и используйте утилиту сжатия gzip, как показано.
sudo ./star.sh -i 0 -d /backups -c gzip -H -u "--warning=none"
Восстановить резервную копию системы Linux
Вы также можете восстановить резервную копию, как в следующей команде.
sudo ./star.sh -i 1 -r /dev/sdb1 -G /dev/sdb -f /backups/backup.tar.xz
где варианты:
-i- указывает режим работы ( 1 означает режим восстановления).
-r- определяет целевой корневой (/) раздел.
-G - определяет раздел grub.
-f - указал путь к файлу резервной копии.
В последнем примере показано, как запустить его в режиме передачи ( 2 ). 
Здесь есть новая опция -b, которая устанавливает загрузочный раздел.
sudo ./star.sh -i 2 -r /dev/sdb2 -b /dev/sdb1 -G /dev/sdb
Кроме того, если вы смонтировали / usr и / var на разных разделах, с учетом предыдущей команды, 
вы можете указать их с помощью -tпереключателя, как показано.
sudo ./star.sh -i 2 -r /dev/sdb2 -b /dev/sdb1 -t "/var=/dev/sdb4 /usr=/dev/sdb3" -G /dev/sdb
Мы только что рассмотрели несколько основных параметров сценария System Tar и Restore , 
вы можете просмотреть все доступные параметры, используя следующую команду.
star.sh --help 
Если вы привыкли к графическому пользовательскому интерфейсу, 
вы можете использовать вместо него GUI-оболочку star-gui.sh . 
Но вам необходимо установить gtkdialog - используемый для создания графических (GTK +) интерфейсов и диалоговых окон 
с использованием сценариев оболочки в Linux.
Системный Tar и Restore Gui
Вы можете найти больше примеров использования командной строки в репозитории System Tar и Restore 

