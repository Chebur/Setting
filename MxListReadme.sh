# sources.list.d
# MX-Linux/mx-repo-list
# https://github.com/MX-Linux
# https://github.com/MX-Linux/mx-repo-list
# https://github.com/MX-Linux/mx-repo-list/blob/master/repos.txt
wget https://codeberg.org/Chebur/Setting/raw/branch/main/MxList.tar.gz
tar xvf MxList.tar.gz && rm -rf MxList.tar.gz
sudo mv mx.list /etc/apt/sources.list.d/