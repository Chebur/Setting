#SystembackSh.AppImage
wget https://codeberg.org/Chebur/Setting/raw/branch/main/SystembackSh.AppImage.tar.gz
tar xvf SystembackSh.AppImage.tar.gz && rm -rf SystembackSh.AppImage.tar.gz 
sudo cp SystembackSh.AppImage/systemback.png /usr/share/icons/
chmod +x SystembackSh.AppImage/BackUpSystembackAppImage.desktop
chmod +x SystembackSh.AppImage/RestoreSystembackAppImage.desktop
cp SystembackSh.AppImage/BackUpSystembackAppImage.desktop ~/.local/share/applications/
cp SystembackSh.AppImage/RestoreSystembackAppImage.desktop ~/.local/share/applications/
chmod +x SystembackSh.AppImage/SystembackSh.AppImage
sudo cp SystembackSh.AppImage/SystembackSh.AppImage /usr/local/bin/
sudo mkdir /home/sblive  
cp SystembackSh.AppImage/SystembackUninstall.sh ~/'Рабочий стол'/    или    ~/'Desktop'/
cp SystembackSh.AppImage/SystembackOption.sh ~/'Рабочий стол'/    или    ~/'Desktop'/
rm -Rf SystembackSh.AppImage/
##################################################################################
sudo apt install -y attr psmisc rsync xterm
wget https://codeberg.org/Chebur/Setting/raw/branch/main/SystembackSh.tar.gz
tar xvf SystembackSh.tar.gz && rm -rf SystembackSh.tar.gz
chmod +x systemback/systemback.sh
sudo cp systemback/systemback.sh /usr/local/bin/
chmod +x systemback/RestoreSystemback.desktop
chmod +x systemback/BackUpSystemback.desktop
cp systemback/RestoreSystemback.desktop ~/.local/share/applications/
cp systemback/BackUpSystemback.desktop ~/.local/share/applications/
sudo cp systemback/systemback.png /usr/share/icons/
cp systemback/SystembackUninstall.sh ~/'Рабочий стол'/   или   ~/'Desktop'/
cp systemback/SystembackOption.sh ~/'Рабочий стол'/   или   ~/'Desktop'/
sudo mkdir /home/sblive                                                      
rm -Rf systemback
##################################################################################
Чтобы удалить все точки восстановления, используйте следующие команды:
cd /home/sblive   
sudo chattr -Rfi SB_*   
sudo rm -rf SB_*   

System backup and restore script for Debian-based distributions v3.4 by Kendek

  Available options:

   -n, --new [NAME]           create a new restore point
   
   -s, --storage              print the current storage directory path and the
   
                              slot
			      
   -l, --list                 list available restore points
   
   -z, --size [INDEX]         calculate the incremental size of the storage slot
   
                              or the apparent size of a restore point
			      
   -r, --restore [INDEX]      perform a system and/or user's configuration files
   
                              restoration
			      
   -m, --repair [INDEX]       same as -r but the target (root) directory will be
   
                              the '/mnt' instead of the '/'
			      
   -e, --rename INDEX [NAME]  rename a restore point
   
   -k, --keep INDEX           set the restore point to be manually removable
                              only
			      
   -d, --remove [INDEX]       manually remove a restore point
   
   