#PulseAudioAlsa
echo 'Устанавливаем звук от  pulseaudio'  #https://www.freedesktop.org/wiki/Software/PulseAudio/
sleep 1s
sudo apt install pulseaudio gstreamer1.0-pulseaudio pulseaudio-utils xfce4-pulseaudio-plugin pavucontrol libpulse-mainloop-glib0 libpulse0 libpulsedsp -y
echo 'Устанавливаем звук от Alsa'   #https://alsa-project.org/wiki/Main_Page
sudo apt install alsa-utils gstreamer1.0-alsa libasound2 libasound2-data libasound2-plugins -y
wget https://gitlab.com/Chebur70/Setting/-/raw/main/PulseAudioAlsa.tar.gz
tar xvf PulseAudioAlsa.tar.gz && rm -rf PulseAudioAlsa.tar.gz
chmod +x PulseAudioAlsa/Pulseaudio.desktop
cp PulseAudioAlsa/Pulseaudio.desktop ~/.config/autostart/
sudo cp PulseAudioAlsa/PulseAudio.svg /usr/share/icons/
rm -Rf PulseAudioAlsa