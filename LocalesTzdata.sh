#!/bin/bash
#"author chebur chebur3133@gmail.com https://prostolinux.my1.ru/index/st/0-11 https://gitlab.com/Chebur70 https://try.gitea.io/Chebur70
#"Огромное Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
#"Огромное Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
#"Огромное Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
#"Огромное Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
#"Огромное Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
#"Огромное Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
#"Огромное Спасибо Systemback edmond https://github.com/fconidi"
#"Нет недостижимых целей,есть высокий коэффициент лени,недостаток смекалкии и запас отговорок"
#"Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
#"Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс! Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
#"Запомните,чтобы ничего не делать, надо уметь делать все" 

sleep 2s
#Настраиваем локали:
sudo dpkg-reconfigure locales
#В качестве локали по умолчанию выбираем
#ru_RU.UTF-8
#Устанавливаем пакетный менеджер aptitude
sudo apt install aptitude
#Ставим консольные шрифты:
aptitude install console-cyrillic
#Обновляем шрифты с помощью команды:
sudo dpkg-reconfigure console-cyrillic
#Изменить часовой пояс
sudo dpkg-reconfigure tzdata
#Перезагружаете компьютер.
sudo reboot 
