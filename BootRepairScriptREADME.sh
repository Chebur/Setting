"Boot-Repair-Восстановление загрузки"

#Install BootRepairScript
			       echo "================ Установка BootRepairScript ================"
			       wget https://gitlab.com/Chebur70/Setting/-/raw/main/BootRepairScript.tar.gz
			       tar xvf BootRepairScript.tar.gz && rm -rf BootRepairScript.tar.gz
			       chmod -R 777 BootRepairScript
                               sudo cp BootRepairScript/BootRepairEFI.sh /opt/
                               sudo cp BootRepairScript/BootRepairPC.sh /opt/
			       sudo cp BootRepairScript/BootRepairEFI.desktop /usr/share/applications/
			       sudo cp BootRepairScript/BootRepairPC.desktop /usr/share/applications/
			       sudo cp BootRepairScript/BootRepair.png /usr/share/icons/
			       rm -Rf BootRepairScript
			       
#Uninstall BootRepairScript 
			       echo "================ Удаление BootRepairScript ================"
                               sudo rm -rf /opt/BootRepairEFI.sh 
                               sudo rm -rf /opt/BootRepairPC.sh
			       sudo rm -rf /usr/share/applications/BootRepairEFI.desktop
			       sudo rm -rf /usr/share/applications/BootRepairPC.desktop 
			       sudo rm -rf  /usr/share/icons/BootRepair.png			       